all: test

test: main.o
	$(CC) -o test main.o

clean:
	rm -rf *.o
	rm -rf test
    